import {Adresse} from './Adresse';

export class Personne {
    private _nom :string;
    private _sexe :string;
    private _adresse :Adresse;

    constructor(nom:string, sexe:string, adresse:Adresse){
        this._nom=nom;
        this._sexe=sexe;
        this._adresse=adresse;
    }

    get nom():string{
        return this._nom;
    }

    set nom(nom:string){
        this._nom=nom;
    }

    get sexe():string{
        return this._sexe;
    }

    set sexe(sexe:string){
        this._sexe=sexe;
    }

    get adresse():Adresse{
        return this._adresse;
    }

    set adresse(adresse :Adresse){
        this._adresse= adresse;
    }


}