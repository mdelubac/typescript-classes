 export class Stagiaire {
private _nom :string;
private _note :number[];

constructor(nom: string, note: number[]){
    this._nom = nom;
    this._note= note;
}

get nom():string {
    return this._nom;
}

set nom(nom :string) {
    this._nom=nom;
}

get note(): number[]{
    return this._note;
}
set note(note: number[]){
    this._note=note;
}
 
calculerMoyenne() : number {
    let addResultat : number =0;
    let moyenne : number =0;
    this._note.forEach(e=>{
        addResultat= addResultat+e;
    })
    moyenne = addResultat/this._note.length;
    return moyenne;
}

trouverMax() : number {
    let max: number = this._note[0];
    this._note.forEach(e =>{
        if (e>max){
            max = e;
        }
    })
    return max;
}

trouverMin() : number {
    let min: number = this._note[0];
    this._note.forEach(e =>{
        if (e<min){
            min = e;
        }
    })
    return min;
}   
}



export class Formation {

    private nomFormation : string;
    private nbJours : number;
    private stagiaires : Stagiaire[];


    constructor(nomFormation : string, nbJours : number, stagiaires : Stagiaire[]){
        this.nomFormation =nomFormation;
        this.nbJours =nbJours;
        this.stagiaires=stagiaires;
    }


    calculerMoyenneFormation() : number {
        let addMoyenne :number =0;
        let moyenne :number =0;
        this.stagiaires.forEach( (e)=>{
            addMoyenne= addMoyenne + e.calculerMoyenne();
        });

        moyenne =addMoyenne/this.stagiaires.length;
        return moyenne;

    }



}




