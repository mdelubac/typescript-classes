import {Adresse} from './Adresse';
import {Personne} from './Personne';
import {ListePersonne} from './ListePersonne';

const ad1 :Adresse =new Adresse('Enclos Jalabert', 'Nimes', 30000);
const ad2 :Adresse =new Adresse('Marne', 'Toulouse', 31400);


const per1 :Personne = new Personne('Marc', 'homme', ad1);
const per2 :Personne = new Personne('Anthony', 'homme', ad2);

const list : Personne[]=[];
list.push(per1);
list.push(per2);
JSON.stringify(list);

const listper1 : ListePersonne = new ListePersonne(list);



console.log(`${JSON.stringify(listper1.findByNom('Anthony'))}`);
console.log(`${JSON.stringify(listper1.findByCodePostale(30000))}`);

// list.forEach((e)=>{
//     console.log(e);
// })