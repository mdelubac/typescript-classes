"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListePersonne = void 0;
var ListePersonne = /** @class */ (function () {
    function ListePersonne(personnes) {
        this._personnes = personnes;
    }
    Object.defineProperty(ListePersonne.prototype, "personnes", {
        get: function () {
            return this._personnes;
        },
        set: function (personnes) {
            this._personnes = personnes;
        },
        enumerable: false,
        configurable: true
    });
    ListePersonne.prototype.findByNom = function (s) {
        for (var _i = 0, _a = this._personnes; _i < _a.length; _i++) {
            var p = _a[_i];
            if (p.nom === s) {
                return p;
            }
        }
        return null;
    };
    ListePersonne.prototype.findByCodePostale = function (cp) {
        for (var _i = 0, _a = this._personnes; _i < _a.length; _i++) {
            var p = _a[_i];
            if (p.adresse.codePostale === cp) {
                return p;
            }
        }
        return null;
    };
    return ListePersonne;
}());
exports.ListePersonne = ListePersonne;
