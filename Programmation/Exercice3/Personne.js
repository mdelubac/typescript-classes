"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Personne = void 0;
var Personne = /** @class */ (function () {
    function Personne(nom, sexe, adresse) {
        this._nom = nom;
        this._sexe = sexe;
        this._adresse = adresse;
    }
    Object.defineProperty(Personne.prototype, "nom", {
        get: function () {
            return this._nom;
        },
        set: function (nom) {
            this._nom = nom;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Personne.prototype, "sexe", {
        get: function () {
            return this._sexe;
        },
        set: function (sexe) {
            this._sexe = sexe;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Personne.prototype, "adresse", {
        get: function () {
            return this._adresse;
        },
        set: function (adresse) {
            this._adresse = adresse;
        },
        enumerable: false,
        configurable: true
    });
    return Personne;
}());
exports.Personne = Personne;
