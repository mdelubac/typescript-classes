import {Stagiaire, Formation} from './exercice2.js';

let tab : number[]=[18,19,20];
let tab2 : number[] =[17, 18, 19];
let tab3 : number[]=[16, 17, 18];

let nom1 : string ='Marc';
let nom2 : string ='Eric';
let nom3 : string ='John';


let s1 : Stagiaire = new Stagiaire(nom1, tab );
let s2 : Stagiaire = new Stagiaire(nom2, tab2);
let s3 : Stagiaire = new  Stagiaire(nom3, tab3);

let stagiaires :Stagiaire []= [];
stagiaires.push(s1);
stagiaires.push(s2);
stagiaires.push(s3);


let f1 = new Formation('INTI', 57, stagiaires);

console.log(`_______________________Exerceice  2a___________________ `)

console.log(`la moyenne de ${s1.nom} est de ${s1.calculerMoyenne()}`);
console.log(`la note minimale de ${s1.nom} est de ${s1.trouverMin()}`);
console.log(`la note maximale de ${s1.nom} est de ${s1.trouverMax()}`);

console.log(`_______________________Exerceice  2b___________________ `)

console.log(`Pour la formation INTI, la moyenne de la promotion est de : ${f1.calculerMoyenneFormation()}`);
    
