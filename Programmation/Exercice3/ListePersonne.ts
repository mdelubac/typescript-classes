import {Personne} from "./Personne";
import {Adresse} from './Adresse';

export class ListePersonne{
    private _personnes :Personne[];

    constructor(personnes :Personne[]){
        this._personnes =personnes;
    }

    get personnes(): Personne[]{
        return this._personnes;
    }

    set personnes(personnes : Personne[]){
        this._personnes=personnes;
    }

    findByNom(s:string):Personne{
       for (let p of this._personnes){
           if(p.nom===s){
               return p;         
           }
       }
       return null;
       
    }

    findByCodePostale(cp:number):Personne{
        for (let p of this._personnes){
            if(p.adresse.codePostale===cp){
              return p;
            }
        }
        return null;
     }
     


}