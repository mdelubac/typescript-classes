export class Adresse {
    private _rue : string;
    private _ville : string;
    private _codePostale :number;


    constructor(rue:string, ville:string, codePostale:number){
        this._rue=rue;
        this._ville=ville;
        this._codePostale =codePostale;

    }

    get rue():string{
        return this._rue;
    }

    set rue(rue:string){
        this._rue=rue;
    }

    get ville():string{
        return this._ville;
    }

    set ville(ville : string){
        this._ville=ville;
    }

    get codePostale(): number{
        return this._codePostale;
    }

    set codePostale(codePostale:number){
        this._codePostale=codePostale;
    }

}