"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Adresse_1 = require("./Adresse");
var Personne_1 = require("./Personne");
var ListePersonne_1 = require("./ListePersonne");
var ad1 = new Adresse_1.Adresse('Enclos Jalabert', 'Nimes', 30000);
var ad2 = new Adresse_1.Adresse('Marne', 'Toulouse', 31400);
var per1 = new Personne_1.Personne('Marc', 'homme', ad1);
var per2 = new Personne_1.Personne('Anthony', 'homme', ad2);
var list = [];
list.push(per1);
list.push(per2);
JSON.stringify(list);
var listper1 = new ListePersonne_1.ListePersonne(list);
console.log("" + JSON.stringify(listper1.findByNom('Anthony')));
console.log("" + JSON.stringify(listper1.findByCodePostale(30000)));
// list.forEach((e)=>{
//     console.log(e);
// })
