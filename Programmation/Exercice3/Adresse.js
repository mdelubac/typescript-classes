"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Adresse = void 0;
var Adresse = /** @class */ (function () {
    function Adresse(rue, ville, codePostale) {
        this._rue = rue;
        this._ville = ville;
        this._codePostale = codePostale;
    }
    Object.defineProperty(Adresse.prototype, "rue", {
        get: function () {
            return this._rue;
        },
        set: function (rue) {
            this._rue = rue;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Adresse.prototype, "ville", {
        get: function () {
            return this._ville;
        },
        set: function (ville) {
            this._ville = ville;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Adresse.prototype, "codePostale", {
        get: function () {
            return this._codePostale;
        },
        set: function (codePostale) {
            this._codePostale = codePostale;
        },
        enumerable: false,
        configurable: true
    });
    return Adresse;
}());
exports.Adresse = Adresse;
