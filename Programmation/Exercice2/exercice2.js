"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Formation = exports.Stagiaire = void 0;
class Stagiaire {
    constructor(nom, note) {
        this._nom = nom;
        this._note = note;
    }
    get nom() {
        return this._nom;
    }
    set nom(nom) {
        this._nom = nom;
    }
    get note() {
        return this._note;
    }
    set note(note) {
        this._note = note;
    }
    calculerMoyenne() {
        let addResultat = 0;
        let moyenne = 0;
        this._note.forEach(e => {
            addResultat = addResultat + e;
        });
        moyenne = addResultat / this._note.length;
        return moyenne;
    }
    trouverMax() {
        let max = this._note[0];
        this._note.forEach(e => {
            if (e > max) {
                max = e;
            }
        });
        return max;
    }
    trouverMin() {
        let min = this._note[0];
        this._note.forEach(e => {
            if (e < min) {
                min = e;
            }
        });
        return min;
    }
}
exports.Stagiaire = Stagiaire;
class Formation {
    constructor(nomFormation, nbJours, stagiaires) {
        this.nomFormation = nomFormation;
        this.nbJours = nbJours;
        this.stagiaires = stagiaires;
    }
    calculerMoyenneFormation() {
        let addMoyenne = 0;
        let moyenne = 0;
        this.stagiaires.forEach((e) => {
            addMoyenne = addMoyenne + e.calculerMoyenne();
        });
        moyenne = addMoyenne / this.stagiaires.length;
        return moyenne;
    }
}
exports.Formation = Formation;
